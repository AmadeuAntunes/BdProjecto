﻿using BDProjecto.Dominio;
using BDProjecto.Dominio.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDProjecto.RepositorioEF
{
    public class UsuarioRepositorioEF : IRepositorio<Usuarios>
    {
        private readonly BD bd;
        public UsuarioRepositorioEF()
        {
            bd = new BD();
        }

        public void Excluir(Usuarios entidade)
        {
            var usuarioExcluir = bd.usuarios.First(x => x.Id == entidade.Id);
            bd.Set<Usuarios>().Remove(entidade);
            bd.SaveChanges();
        }

        public Usuarios ListarPorId(string id)
        {
            int ID = 0;
            Int32.TryParse(id, out ID);
            return bd.usuarios.First(x => x.Id == ID);
        }

      


        public IEnumerable<Usuarios> ListarTodos()
        {
            return bd.usuarios;
        }

        public void Salvar(Usuarios entidade)
        {
            if(entidade.Id > 0)
            {
                var usuarioAlterar = bd.usuarios.First(x => x.Id == entidade.Id);
                usuarioAlterar.Nome = entidade.Nome;
                usuarioAlterar.Cargo = entidade.Cargo;
                usuarioAlterar.Data = entidade.Data;

            }
            else
            {
                bd.usuarios.Add(entidade);
            }

            bd.SaveChanges();
        }
    }
}
