﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BDProjecto.Dominio
{
    public class Usuarios
    {
        public int Id { get; set; }
        [DisplayName("Nome")]
        [Required(ErrorMessage = "Preencha o nome do utilizador")]
        public string Nome { get; set; }
        [DisplayName("Cargo")]
        [Required(ErrorMessage = "Preencha o cargo do utilizador")]
        public string Cargo { get; set; }
        [DisplayName("Data")]
        [Required(ErrorMessage = "Preencha a data de registo")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Data { get; set; }
    }
}
