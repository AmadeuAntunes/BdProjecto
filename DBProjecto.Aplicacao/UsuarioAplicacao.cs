﻿using BDProjecto.Dominio;
using System.Collections.Generic;
using BDProjecto.RepositorioADO;
using BDProjecto.Dominio.contrato;

namespace BDProjecto.Aplicacao
{
    public class UsuarioAplicacao
    {
        private readonly IRepositorio<Usuarios> repositorio;

        public UsuarioAplicacao(IRepositorio<Usuarios> rep)
        {
            repositorio = rep;
        }

        public void Salvar(Usuarios usuarios)
        {
            repositorio.Salvar(usuarios);
        }

        public void Excluir(Usuarios usuarios)
        {
            repositorio.Excluir(usuarios);
        }

        public IEnumerable<Usuarios> ListarTodos()
        {
            return repositorio.ListarTodos();
          
        }

        public Usuarios ListarPorId(string id)
        {
            return repositorio.ListarPorId(id);

        }

    }
}
