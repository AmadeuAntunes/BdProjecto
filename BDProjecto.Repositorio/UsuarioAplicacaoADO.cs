﻿using BDProjecto.Dominio;
using BDProjecto.Dominio.contrato;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace BDProjecto.RepositorioADO
{
    public class UsuarioAplicacaoADO: IRepositorio<Usuarios>
    {
        private bd bd;

        private void Insert(Usuarios usuarios)
        {
            var strQuery = "";
            strQuery += "INSERT INTO usuarios(Nome, Cargo, Data)";
            strQuery += string.Format(" Values('{0}', '{1}','{2}')", usuarios.Nome, usuarios.Cargo, usuarios.Data
                );

            using (bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

        private void Alterar(Usuarios usuarios)
        {
            var strQuery = "";
            strQuery += "UPDATE usuarios SET ";
            strQuery += string.Format("Nome ='{0}', ", usuarios.Nome);
            strQuery += string.Format("Cargo ='{0}', ", usuarios.Cargo);
            strQuery += string.Format("Date ='{0}' ", usuarios.Data);
            strQuery += string.Format("WHERE Id = '{0}'", usuarios.Id);

            using (bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

      

        public void Salvar(Usuarios usuarios)
        {
            if(usuarios.Id > 0)
            {
                Alterar(usuarios);
            }
            else
            {
                Insert(usuarios);
            }
        }

        public void Excluir(Usuarios usuario)
        {
            var strQuery = "";
            strQuery += "DELETE FROM usuarios WHERE";
            strQuery += string.Format(" Id = {0}", usuario.Id
                );

            using (bd = new bd())
            {
                bd.ExecutaComando(strQuery);
            }
        }

        public IEnumerable<Usuarios> ListarTodos()
        {
            using (bd = new bd())
            {
                string strQuerySelect = "Select * From usuarios";

               return  ReaderEmlista(bd.ExecutaComandoComRetorno(strQuerySelect)); 
            }
          
        }

        public Usuarios ListarPorId(string id)
        {
            if(id != null)
            {
                using (bd = new bd())
                {
                    string strQuerySelect = string.Format("Select * From usuarios where Id={0}", id);

                    return (ReaderEmlista(bd.ExecutaComandoComRetorno(strQuerySelect))).FirstOrDefault();
                }

            }
            return null;
          
        }


        private List<Usuarios> ReaderEmlista(SqlDataReader reader)
        {
            List<Usuarios> Lista = new List<Usuarios>();
            while (reader.Read())
            {
                Lista.Add(new Usuarios
                {
                    Id = int.Parse(reader["Id"].ToString()),
                    Nome = reader["Nome"].ToString(),
                    Cargo = reader["Cargo"].ToString(),
                    Data = DateTime.Parse(reader["Data"].ToString())
                });
               
            }
            reader.Close();
            return Lista;
        }
    }
}
