﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BDProjecto.RepositorioADO
{
    public class bd : IDisposable
    {
        private readonly SqlConnection conexao;

        public bd()
        {
            bool exception = false;
            conexao = new SqlConnection(ConfigurationManager.ConnectionStrings["conexaoBD"].ConnectionString);
            try
            {
                conexao.Open();
            }
            catch (Exception)
            {
                exception = true;
                throw new ArgumentException("Erro na conexão com o servidor");
            }
        }

        public void ExecutaComando(string strQuery)
        {


            var cmdComando = new SqlCommand
            {
                CommandText = strQuery,
                CommandType = CommandType.Text,
                Connection = conexao
            };
            cmdComando.ExecuteNonQuery();

        }

        public SqlDataReader ExecutaComandoComRetorno(string strQuery)
        {
            SqlCommand cmdComandoSelect = new SqlCommand(strQuery, conexao);
            return cmdComandoSelect.ExecuteReader();
        }

        public void Dispose()
        {
            if (conexao.State == ConnectionState.Open)
                conexao.Close();
        }
    }
}
