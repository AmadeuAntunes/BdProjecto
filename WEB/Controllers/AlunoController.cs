﻿using BDProjecto.Aplicacao;
using BDProjecto.Dominio;
using DBProjecto.Aplicacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WEB.Controllers
{
    public class AlunoController : Controller
    {
       // private UsuarioAplicacao appUsuario;
        // GET: Aluno
        public AlunoController()
        {
           // appUsuario = UsuarioAplicacaoConstrutor.UsuarioApEF();
        }

        public ActionResult Index()
        {
           var appUsuario = UsuarioAplicacaoConstrutor.UsuarioApADO();
            var listaUsuarios = appUsuario.ListarTodos();
            return View(listaUsuarios);
        }

        public ActionResult Criar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Criar(Usuarios usuario)
        {
            if (ModelState.IsValid)
            {
                var appUsuario = UsuarioAplicacaoConstrutor.UsuarioApADO();
                appUsuario.Salvar(usuario);
                return RedirectToAction("index");
            }
            return View();
        }

       
        public ActionResult Editar(string id)
        {
            var appUsuario = UsuarioAplicacaoConstrutor.UsuarioApADO();
            Usuarios usuario = null;
                   usuario = appUsuario.ListarPorId(id);
                if (usuario == null)
                return HttpNotFound();

            return View(usuario);
        }

        [HttpPost]
        public ActionResult Editar(Usuarios usuario)
        {
            var appUsuario = UsuarioAplicacaoConstrutor.UsuarioApADO();
            appUsuario.Salvar(usuario);
            return RedirectToAction("index");
        }

        public ActionResult Detalhes(string id)
        {
            var appUsuario = UsuarioAplicacaoConstrutor.UsuarioApADO();
            Usuarios usuario = null;
            usuario = appUsuario.ListarPorId(id);
            if (usuario == null)
                return HttpNotFound();

            return View(usuario);
        }

        public ActionResult Apagar(string id)
        {
            var appUsuario = UsuarioAplicacaoConstrutor.UsuarioApADO();
            Usuarios usuario = null;
            usuario = appUsuario.ListarPorId(id);
            if (usuario == null)
                return HttpNotFound();

            return View(usuario);
        }


        [HttpPost, ActionName("Apagar")]
        public ActionResult ApagarConfirmar(string id)
        {
            var appUsuario = UsuarioAplicacaoConstrutor.UsuarioApADO();
            var usuario = appUsuario.ListarPorId(id);
            appUsuario.Excluir(usuario);
            return RedirectToAction("index");
        }

     






    }
}